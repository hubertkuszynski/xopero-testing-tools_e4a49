﻿using System;
using FileGeneratorLib;
using System.IO;

namespace FileGenerator
{
    public class Program
    {
        [STAThread]
        public static void Main(string[] args)
        {
            Properties properties = new Properties(); // contains test files parameters
            bool help = false; // states if program only displayed help
            bool doable = false; // states if there is enought parameters to generate test files
            bool macro = false; // states if settings should be read from args[0]
            #region Determine execution type [normal/help/interactive/macro]
            if (args.Length == 0)
            {
                Console.WriteLine("Did you mean:");
                Console.WriteLine("    -i    interactive mode");
                Console.WriteLine("See -help for help");
            }
            else if (args.Length == 1)
            {
                if (Properties.Matches(args[0], "i"))
                {
                    //todo: new, full interactive mode (manual input)
                    properties.stopwatch.Start();
                }
                #region Help
                else if (Properties.Matches(args[0], new string[] { "?", "h", "help" })) // display help
                {
                    Console.WriteLine("FileGenerator");
                    Console.WriteLine("Created by:  Kacper Pastusiak");
                    Console.WriteLine("Upgraded by: Robert Wojtaszak");
                    Console.WriteLine("Last editor: Piotr Pogorzelec");
                    Console.WriteLine("\nList of parameters:");
                    Console.WriteLine("# Required #");
                    Console.WriteLine("  -p        [string] Path to working directory");
                    Console.WriteLine("  -min      [int]    Minimum size of file to generate");
                    Console.WriteLine("  -max      [int]    Maximum size of file to generate");
                    Console.WriteLine("  -u        [string] Unit size to be used [b/kb/mb/gb]");
                    Console.WriteLine("# Optional #");
                    Console.WriteLine("  -rp       [string] Path to report directory");
                    Console.WriteLine("  -s                 Suppress all messages (overrides -v/-d except warning if -s -v -d are used simult");
                    Console.WriteLine("  -v                 Display more info and current progress (will slow down execution for large tests).");
                    Console.WriteLine("  -d                 Enable debug");
                    Console.WriteLine("# CREATE MODE #");
                    Console.WriteLine("# Required #");
                    Console.WriteLine("  -c        [int]    Number of files to generate");
                    Console.WriteLine("  -dc       [int]    Number of folders to generate");
                    Console.WriteLine("# Optional #");
                    Console.WriteLine("  -roff              Disables random names (names will start with 0 and end with c-1)");
                    Console.WriteLine("  -dd       [int]    Directory depth");
                    Console.WriteLine("  -dp       [int]    Propability of placing directory within another directory");
                    Console.WriteLine("  -fnl      [int]    File name length [min: 1 max: 260]");
                    Console.WriteLine("  -dnl      [int]    Directory name length [min: 1 max: 248]");
                    Console.WriteLine("  -fel      [string] File extension length [min: 0 max: 258]");
                    Console.WriteLine("  -ext      [string] Defines extension to use [mutually exclusive with -fel, takes precedence]");
                    Console.WriteLine("  -chars    [string] Characters used for filenames.");
                    Console.WriteLine("  -extchars [string] Characters used for extensions.");
                    Console.WriteLine("# CHANGE MODE #");
                    Console.WriteLine("# Optional #");
                    Console.WriteLine("  -c        [int]    Number of files to change [default: all]");
                    Console.WriteLine("  -imin     [int]    Minimum number of change per file [default: 1]");
                    Console.WriteLine("  -imax     [int]    Maximum number of changes per file [default: 5]");
                    Console.WriteLine("  -fp       [int]    Change probability [default: 100]");
                    Console.WriteLine("\nBy default program will end with a string \"Done.\"");
                    help = true;
                }
                else
                {
                    macro = true; // if it isn't help/interactive call then load args from args[0]
                }
                #endregion
            }
            #endregion
            if (!help) // if program displayed help further actions are unnecessary
            {
                #region Read parameters
                try
                {
                    if (!macro)
                    {
                        properties.Load(args, ref doable);
                    }
                    else
                    {
                        properties.Load(File.ReadAllLines(args[0]), ref doable);
                    }
                }
                catch (Exception e)
                {
                    if (!properties.Silent)
                    {
                        Properties.DisplayWarning(e.Message);
                        if (properties.Debug) Properties.DisplayWarning(e.StackTrace);
                    }
                    Environment.Exit(-1);
                }
                #endregion
                #region Execute
                if (doable)
                {
                    try
                    {
                        #region [Debug] Warnings
                        if (properties.Debug)
                        {
                            if (properties.Verbose && properties.Silent)
                            {
                                Properties.DisplayWarning("[Debug]Warning: you are suppresing all output with -s!");
                            }
                            if (properties.FileCount > 100000 || properties.DirectoryCount > 100000)
                            {
                                Properties.DisplayWarning("[Debug]Warning: you are about to generate " +
                                    $"\n{properties.FileCount} files" +
                                    $"\n{properties.DirectoryCount} folders" +
                                    "\nIt will take very long time to both generate and/or remove those. Press any key to continue or terminate with CTRL+C.", true);
                            }
                        }
                        #endregion
                        #region Mode execution
                        Random random = new Random();
                        if (properties.GenerateNewFiles)
                        {
                            #region Generating folders
                            DirectoryManager folderManager = new DirectoryManager(properties, random);
                            folderManager.Generate();
                            #endregion
                            #region Generating files
                            FileManager fileManager = new FileManager(properties, random, false, folderManager.Folders);
                            fileManager.Generate();
                            #endregion
                        }
                        else
                        {
                            #region Change
                            FileManager fileManager = new FileManager(properties, random, true);
                            fileManager.Change();
                            #endregion
                        }
                        if (!properties.Silent) Console.WriteLine("Done.");
                        #endregion
                    }
                    catch (Exception e)
                    {
                        Properties.DisplayWarning(e.Message);
                        if (properties.Debug) Properties.DisplayWarning(e.StackTrace);
                        Environment.Exit(-1);
                    }
                }
                #endregion
            }
        }
    }
}
