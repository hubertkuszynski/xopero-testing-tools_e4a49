﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;

namespace FileGeneratorLib
{
    /// <summary>
    /// Used to store program settings.
    /// </summary>
    public class Properties
    {
        public Stopwatch stopwatch = new Stopwatch();

        #region Data
        #region Constants
        public const int MaxLength = 259; // Maximum file name is 260, maximum folder name is 248; -1 for '.'
        public const string sanityCheckFailure = "Please do not insert invalid values directly into program memory. If you did not do any of the like there is a high risk that unathorized program is adding its code to running executables as this exception will be thrown only in logic-impossible scenarios.";
        public const string TokenFileName = ".testToken";
        #endregion
        #region Enums
        public enum UnitSize
        {
            B,
            KB,
            MB,
            GB
        }

        public enum ModeType
        {
            add,
            inc
        }
        #endregion
        #region Variables
        private string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&()_+=[]{};',.`~ ";
        private string extChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        private string folderPath = string.Empty;
        private string reportPath = string.Empty;
        private string ext = string.Empty;
        private string[] exts;
        #endregion
        #region Properties
        public bool Debug { get; set; } = false;
        public UnitSize DataSize { get; set; }
        public int FileCount { get; set; } = -1;
        public int DirectoryCount { get; set; } = -1;
        public int MaxDirectoryDepth { get; set; } = 10;
        public int DirectoryDepthPropability { get; set; } = 50;
        public int Max { get; set; } = -1;
        public int Min { get; set; } = -1;
        public int MaxChanged { get; set; } = 5;
        public int MinChanged { get; set; } = 1;
        public int MaxFileNameLength { get; set; } = 50;
        public int MinFileNameLength { get; set; } = 1;
        public int MaxDirectoryNameLength { get; set; } = 10;
        public int MinDirectoryNameLength { get; set; } = 1;
        public int FileExtensionLength { get; set; } = -1;
        public bool GenerateNewFiles { get; set; } = true;
        public int BufferSize { get; set; } = 1024 * 1024 * 8;
        public int PercentToChange { get; set; } = 100;
        public bool RandomNameMode { get; set; } = true;
        public bool TextMode { get; set; } = false;
        public bool Silent { get; set; } = false;
        public bool Verbose { get; set; } = false;
        public bool Recursive { get; set; } = true;
        public bool DoNotReport { get; set; } = false;
        public string[] args { get; set; }
        public string Ext
        {
            get { return ext; }
            set
            {
                ext = value.StartsWith(".")
                    ? value.Substring(1)
                    : value;
            }
        }
        public string ReportPath
        {
            get
            {
                return reportPath == string.Empty ? GetRootFolder(folderPath) : reportPath;
            }
            set
            {
                reportPath = value.EndsWith($"{Path.DirectorySeparatorChar}")
                    ? value
                    : value + Path.DirectorySeparatorChar;
            }
        }

        public string FolderPath
        {
            get { return folderPath; }
            set
            {
                folderPath = value.EndsWith($"{Path.DirectorySeparatorChar}")
                    ? value
                    : value + Path.DirectorySeparatorChar;
            }
        }
        public string Chars
        {
            get
            {
                return chars;
            }
            set
            {
                SanityCheckAllowed(value);
                chars = value;
            }
        }
        public string ExtensionChars
        {
            get
            {
                return extChars;
            }
            set
            {
                SanityCheckAllowed(value, true);
                extChars = value;
            }
        }
        #endregion
        #region Setters (those methods were created since properties does not allow assigment of value of different type and error messages are a must)
        /// <summary>
        /// Ensures that integer match requirements.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="name"></param>        
        /// <returns></returns>
        private int SetInteger(string input, string name, int min = 1, int max = Int32.MaxValue)
        {
            int value = -1;
            try
            {
                value = Convert.ToInt32(input);
            }
            catch
            {
                throw new Exception($"{name} must be integer.");
            }
            if (value < min)
            {
                throw new Exception($"{name} must be higher than {min}.");
            }
            if (value > max)
            {
                throw new Exception($"{name} must be less than {max}.");
            }
            return value;
        }
        public void SetDataSize(string value)
        {
            switch (value.ToLower())
            {
                case "b":
                case "-b":
                    DataSize = UnitSize.B;
                    break;
                case "kb":
                case "-kb":
                    DataSize = UnitSize.KB;
                    break;
                case "mb":
                case "-mb":
                    DataSize = UnitSize.MB;
                    break;
                case "gb":
                case "-gb":
                    DataSize = UnitSize.GB;
                    break;
                default:
                    throw new Exception("Unsupported data size.");
            }
        }
        public void SetExts(string exts)
        {
            this.exts = exts.Split(',');
        }
        public void SetMax(string input)
        {
            Max = SetInteger(input, "Size of file to generate");
        }
        public void SetMin(string input)
        {
            Min = SetInteger(input, "Size of file to generate");
        }
        public void SetChangedMin(string input)
        {
            MinChanged = SetInteger(input, "Minimum quantity of changes in file");
        }
        public void SetChangedMax(string input)
        {
            MaxChanged = SetInteger(input, "Maximum quantity of changes in file");
        }
        public void SetFileNameLength(string input)
        {
            try
            {
                int value = Convert.ToInt32(input);
                if (Ext.Length + 2 > value) { throw new Exception("Requested filename length is too short for specified extension name."); }
                if (Convert.ToInt32(input) < 1) throw new Exception("File name length must be integer greater than 1.");
                if (exts != null && exts.Length > 0)
                {
                    foreach(string s in exts)
                    {
                        if(s.Length + 2 > value)
                        {
                            throw new Exception($"Requested filename length is too short for one of specified extensions: {s}");
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
            SetMinFileNameLength(input);
            SetMaxFileNameLength(input);
            SanityCheck();
        }
        public void SetMaxFileNameLength(string input)
        {
            MaxFileNameLength = SetInteger(input, "Maximum file name length");
            SanityCheck();
        }
        public void SetMinFileNameLength(string input)
        {
            MinFileNameLength = SetInteger(input, "Minimum file name length", 1, MaxFileNameLength);
            SanityCheck();
        }
        public void SetFileExtensionLength(string input)
        {
            FileExtensionLength = SetInteger(input, "File extension length");
            SanityCheck();
        }
        public void SetNumberOfFiles(string input)
        {
            FileCount = SetInteger(input, "Number of files");
        }
        public void SetMaxDirectoryDepth(string input)
        {
            MaxDirectoryDepth = SetInteger(input, "Maximum depth of folders");
            SanityCheck();
        }
        public void SetNumberOfDirectoriesToCreate(string input)
        {
            DirectoryCount = SetInteger(input, "Number of folders", 0);
        }
        public void SetFolderDepthPropability(string input)
        {
            DirectoryDepthPropability = SetInteger(input, "Propability of creating folder within another generated folder", 0, 100);
        }
        public void SetMaxDirectoryNameLength(string input)
        {
            MaxDirectoryNameLength = SetInteger(input, "Maximum folder name length");
            SanityCheck();
        }
        public void SetMinDirectoryNameLength(string input)
        {
            MinDirectoryNameLength = SetInteger(input, "Minimum folder name length", 1, MaxDirectoryNameLength);
            SanityCheck();
        }
        public void SetDirectoryNameLength(string input)
        {
            SetMinDirectoryNameLength(input);
            SetMaxDirectoryNameLength(input);
            SanityCheck();
        }
        public void SetBufferSize(string input)
        {
            BufferSize = SetInteger(input, "Buffer size", 1024 * 1024);
        }
        private void SetPercentToChange(string input)
        {
            PercentToChange = SetInteger(input, "Procent of changed files", 0, 100);
        }
        public void SetGenerateNewFiles(string v)
        {
            if (v.ToString() == false.ToString())
            {
                GenerateNewFiles = false;
            }
        }
        #endregion
        #region Getters
        public int GetExtsCount()
        {
            int result = 0;
            if (exts != null) result = exts.Length;
            return result;
        }
        public string[] GetExts()
        {
            return exts;
        }
        /// <summary>
        /// Converts size unit to integer describing multiplication of Byte.
        /// </summary>
        /// <returns>Size unit as integer.</returns>
        public int GetSizeUnit()
        {
            int result;
            switch (DataSize)
            {
                case UnitSize.B:
                    result = 1;
                    break;
                case UnitSize.KB:
                    result = 1024;
                    break;
                case UnitSize.MB:
                    result = 1024 * 1024;
                    break;
                case UnitSize.GB:
                    result = 1024 * 1024 * 1024;
                    break;
                default:
                    throw new Exception(sanityCheckFailure);
            }
            return result;
        }
        #endregion
        #region Helpers
        public static void DisplayWarning(string message, bool interactive = false)
        {
            ConsoleColor prev = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.ForegroundColor = prev;
            if (interactive)
            {
                Console.ReadKey();
            }
        }

        private void SanityCheckAllowed(string input, bool extChars = false)
        {
            string type = extChars ? "Extension" : "Filename";
            List<char> illegals = new List<char>();
            illegals.AddRange(Path.GetInvalidFileNameChars());
            illegals.AddRange(Path.GetInvalidPathChars());
            foreach (char c in illegals)
            {
                if (input.Contains(c.ToString()))
                {
                    throw new Exception($"{type} charset contains illegal character: '{c}'");
                }
            }
        }

        /// <summary>
        /// Ensures no "insane" settings pass.
        /// </summary>
        public void SanityCheck()
        {
            if (FileExtensionLength + MaxFileNameLength + MaxDirectoryDepth * MaxDirectoryNameLength > MaxLength)
            {
                throw new Exception("Potential names could exceed maximum path length allowed by system.");
            }
            if (!RandomNameMode && MaxDirectoryNameLength < DirectoryCount.ToString().Length)
            {
                throw new Exception("Maximum directory name length is too short for requested number of directories to generate.");
            }
        }

        private string GetRootFolder(string path)
        {
            string temp = path.Substring(0, path.LastIndexOf(Path.DirectorySeparatorChar));
            return temp.Substring(0, temp.LastIndexOf(Path.DirectorySeparatorChar));
        }
        #endregion
        #endregion
        #region Loading
        public void Load(string[] args, ref bool doable)
        {
            List<string> arguments = new List<string>();
            this.args = args;
            arguments.InsertRange(0, args);
            arguments.Add("-1");
            #region Global required
            FolderPath = arguments[
                    ThrowIfNotFound(
                        arguments.FindIndex(m => Matches(m, "p")), "p") + 1];
            SetMin(arguments[
                    ThrowIfNotFound(
                    arguments.FindIndex(m => Matches(m, "min")), "min") + 1]);
            SetMax(arguments[
                    ThrowIfNotFound(
                arguments.FindIndex(m => Matches(m, "max")), "max") + 1]);
            SetDataSize(arguments[
                    ThrowIfNotFound(
                arguments.FindIndex(m => Matches(m, "u")), "u") + 1]);
            #endregion
            #region Global optional
            if (arguments.Exists(m => Matches(m, "rp")))
            {
                ReportPath = arguments[arguments.FindIndex(m => Matches(m, "rp")) + 1];
            }
            Debug = arguments.Exists(m => Matches(m, "d"));
            Silent = arguments.Exists(m => Matches(m, "s"));
            Verbose = arguments.Exists(m => Matches(m, "v"));
            DoNotReport = arguments.Exists(m => Matches(m, "noreport"));
            TextMode = arguments.Exists(m => Matches(m, "text"));
            #endregion
            if (arguments.Exists(m => Matches(m, "mode")))
            {
                GenerateNewFiles = false;
                doable = true;
                #region Optional
                if (arguments.Exists(m => Matches(m, "c")))
                {
                    SetNumberOfFiles(arguments[arguments.FindIndex(m => Matches(m, "c")) + 1]);
                }
                if (arguments.Exists(m => Matches(m, "imin")))
                {
                    SetChangedMin(arguments[arguments.FindIndex(m => Matches(m, "imin")) + 1]);
                }
                if (arguments.Exists(m => Matches(m, "imax")))
                {
                    SetChangedMax(arguments[arguments.FindIndex(m => Matches(m, "imax")) + 1]);
                }
                if (arguments.Exists(m => Matches(m, "fp")))
                {
                    SetPercentToChange(arguments[arguments.FindIndex(m => Matches(m, "fp")) + 1]);
                }
                Recursive = !arguments.Exists(m => Matches(m, "norec"));
                #endregion
            }
            else
            {
                #region Required
                SetNumberOfFiles(arguments[
                        ThrowIfNotFound(
                            arguments.FindIndex(m => Matches(m, "c")), "c") + 1]);
                SetNumberOfDirectoriesToCreate(arguments[
                        ThrowIfNotFound(
                            arguments.FindIndex(m => Matches(m, "dc")),
                            "dc") + 1]);
                #endregion
                doable = true;
                #region Optional
                if (arguments.Exists(m => Matches(m, "ext")))
                {
                    Ext = arguments[arguments.FindIndex(m => Matches(m, "ext")) + 1];
                }
                if (arguments.Exists(m => Matches(m, "exts")))
                {
                    SetExts(arguments[arguments.FindIndex(m => Matches(m, "exts")) + 1]);
                }
                if (arguments.Exists(m => Matches(m, "chars")))
                {
                    Chars = arguments[arguments.FindIndex(m => Matches(m, "chars")) + 1];
                }
                if (arguments.Exists(m => Matches(m, "extchars")))
                {
                    ExtensionChars = arguments[arguments.FindIndex(m => Matches(m, "extchars")) + 1];
                }
                RandomNameMode = !arguments.Exists(m => Matches(m, "roff"));
                if (arguments.Exists(m => Matches(m, "dd")))
                {
                    SetMaxDirectoryDepth(arguments[arguments.FindIndex(m => Matches(m, "dd")) + 1]);
                }
                if (arguments.Exists(m => Matches(m, "dp")))
                {
                    SetFolderDepthPropability(arguments[arguments.FindIndex(m => Matches(m, "dp")) + 1]);
                }
                if (arguments.Exists(m => Matches(m, "maxdnl")))
                {
                    SetMaxDirectoryNameLength(arguments[arguments.FindIndex(m => Matches(m, "maxdnl")) + 1]);
                }
                if (arguments.Exists(m => Matches(m, "mindnl")))
                {
                    SetMinDirectoryNameLength(arguments[arguments.FindIndex(m => Matches(m, "mindnl")) + 1]);
                }
                if (arguments.Exists(m => Matches(m, "dnl")))
                {
                    SetDirectoryNameLength(arguments[arguments.FindIndex(m => Matches(m, "dnl")) + 1]);
                }
                if (arguments.Exists(m => Matches(m, "maxfnl")))
                {
                    SetMaxFileNameLength(arguments[arguments.FindIndex(m => Matches(m, "maxfnl")) + 1]);
                }
                if (arguments.Exists(m => Matches(m, "minfnl")))
                {
                    SetMinFileNameLength(arguments[arguments.FindIndex(m => Matches(m, "minfnl")) + 1]);
                }
                if (arguments.Exists(m => Matches(m, "fnl")))
                {
                    SetFileNameLength(arguments[arguments.FindIndex(m => Matches(m, "fnl")) + 1]);
                }
                if (arguments.Exists(m => Matches(m, new string[] { "fel", "enl" })))
                {
                    SetFileExtensionLength(arguments[arguments.FindIndex(m => Matches(m, new string[] { "fel", "enl" })) + 1]);
                }
                #endregion

            }
            stopwatch.Start();
        }

        /// <summary>
        /// React to failure of List.FindIndex(match) with Exception containg meaningful message.
        /// </summary>
        /// <param name="v">Index</param>
        /// <param name="name">Name of missing parameter.</param>
        /// <returns>Index</returns>
        private int ThrowIfNotFound(int v, string name)
        {
            if (v == -1)
            {
                throw new Exception($"Missing required parameter: {name}");
            }
            return v;
        }

        public static bool Matches(string m, string[] options)
        {
            bool match = false;
            Regex regex;
            foreach (string option in options)
            {
                regex = new Regex($"^[-/]*{option.ToLower()}$");
                if (regex.IsMatch(m.ToLower())) { match = true; break; }
            }
            return match;
        }

        public static bool Matches(string m, string option)
        {
            bool match = false;
            Regex regex;
            regex = new Regex($"^[-/]*{option.ToLower()}$");
            if (regex.IsMatch(m.ToLower())) { match = true; }
            return match;
        }
        #endregion
    }
}
