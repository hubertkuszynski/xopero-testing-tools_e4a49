﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;

namespace XoperoTestingTools.Logs
{
    public class Logger
    {
        public string InfoWindowLog(string logMessage)
        {
            logMessage = "[" + DateTime.Now.ToShortDateString()+ " " + DateTime.Now.ToShortTimeString() + "]" + " - " + logMessage + "\n";
            return logMessage;
        }
        public void DebugFileLog(string logMessage)
        {
            if(!Directory.Exists(Environment.CurrentDirectory + "\\Logs"))
            {
                Directory.CreateDirectory(Environment.CurrentDirectory + "\\Logs");
            }

            try
            {
                using (var writer = File.AppendText(Environment.CurrentDirectory + "\\Logs\\" + DateTime.Now.ToShortDateString() + "-" + Environment.MachineName + ".txt"))
                {
                    AppendDebugLog(logMessage, writer);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        #region private methods
        private void AppendDebugLog(string logMessage, TextWriter txtWriter)
        {
            try
            {
                txtWriter.Write("\n");
                txtWriter.WriteLine("[ {0} {1} ] : {2}", DateTime.Now.ToShortDateString(), DateTime.Now.ToLongTimeString(), logMessage);
                txtWriter.Close();
            }
            catch (Exception e)
            {
                DebugFileLog(e.ToString());
            }
        }
        #endregion
    }
}
