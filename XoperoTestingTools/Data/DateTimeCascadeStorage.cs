﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XoperoTestingTools.Data
{
    public class DateTimeCascadeStorage
    {
        public readonly List<DateTimeParameters> _List = new List<DateTimeParameters>();
        public double CascadeInterval { get; set; }

        public void AddToList(DateTimeParameters cascadeStorage)
        {
            _List.Add(cascadeStorage);
        }
    }
}
